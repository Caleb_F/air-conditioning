package com.example.baseweather;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.view.Display;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.baseweather.databinding.ActivityMainBinding;
import com.example.baseweather.ui.SecondActivity;
import com.example.baseweather.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String[] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private ActivityMainBinding binding;
    private BroadcastReceiver receiver;
    private static final int REQUEST_CODE = 0x0001;
    private ValueAnimator valueAnimator0TO1, valueAnimator1TO0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        requestPe();
        initService();
        // 初始化并注册广播接收器
        initBr();
        initAni();
    }

    private void initAni() {
        valueAnimator0TO1 = ValueAnimator.ofFloat(0, 1);
        valueAnimator1TO0 = ValueAnimator.ofFloat(1, 0);

        valueAnimator0TO1.setDuration(500);
        valueAnimator0TO1.setStartDelay(2000);

        valueAnimator1TO0.setDuration(500);
        valueAnimator1TO0.setStartDelay(2000);
    }

    //初始化广播
    private void initBr() {
        IntentFilter filter = new IntentFilter(this.getPackageName() + "1");
        receiver = new br();
        registerReceiver(receiver, filter);
    }


    class br extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            switch (message) {
                case "OFF":
                    MainActivity.this.finish();
                    break;
                //外循环
                case "OUT":
                    binding.btnTemp.setAlpha(0);
                    binding.rvContainer.setAlpha(1);
                    binding.ivLoop.setImageResource(R.drawable.outside_loop);
                    startAnimate();
                    break;
                //内循环
                case "IN":
                    binding.btnTemp.setAlpha(0);
                    binding.rvContainer.setAlpha(1);
                    binding.ivLoop.setImageResource(R.drawable.inside_loop);
                    startAnimate();
                    break;
            }

        }
    }

    private void startAnimate() {
        valueAnimator0TO1.addUpdateListener(animation -> {
            float animatedValue = (float) animation.getAnimatedValue();
            binding.btnTemp.setAlpha(animatedValue);
        });
        valueAnimator0TO1.start();

        valueAnimator1TO0.addUpdateListener(animation -> {
            float animatedValue = (float) animation.getAnimatedValue();
            binding.rvContainer.setAlpha(animatedValue);
        });

        valueAnimator1TO0.start();
    }


    private void requestPe() {
        ArrayList<String> strings = new ArrayList<>();
        Arrays.stream(permissions).forEach(item -> {
            if (ContextCompat.checkSelfPermission(this, item) != PackageManager.PERMISSION_GRANTED) {
                strings.add(item);
            }
        });
        //全部通过
        if (strings.size() == 0) {
            CommonUtils.showToast("全部权限申请已通过");
        } else {
            requestPermissions(strings.toArray(new String[0]), REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //请求码同且请求通过
        if (requestCode == REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            CommonUtils.showToast("全部权限申请已通过");
        }

    }

    private void initService() {
        //获取屏幕管理类
        DisplayManager displayManager = (DisplayManager) this.getSystemService(Context.DISPLAY_SERVICE);
        Display[] displays = displayManager.getDisplays();
        if (displays.length < 3) {
            CommonUtils.showToast("当前未配置仪表盘");
        } else {
            jumpToAnotherPage(displays[2]);
        }
    }

    private void jumpToAnotherPage(Display display) {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // 创建ActivityOptions，指定在副屏上启动
        ActivityOptions options = ActivityOptions.makeBasic();
        options.setLaunchDisplayId(display.getDisplayId());
        // 启动Activity
        startActivity(intent, options.toBundle());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}