package com.example.baseweather;


import android.app.Application;
import android.content.Context;
import android.os.Handler;


public class MyApplication extends Application {
    private static MyApplication myApplication;
    private static Handler mainHandler;


    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
        //Ac -- handler;
        mainHandler = new Handler(getMainLooper());
    }

    public static MyApplication getInstance() {
        return myApplication;
    }

    public static Context getContext() {
        return myApplication.getApplicationContext();
    }

    /**
     * 子线程切换主线程的方法
     */
    public static Handler getHandler() {
        return mainHandler;
    }





}
