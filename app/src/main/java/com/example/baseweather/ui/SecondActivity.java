package com.example.baseweather.ui;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.view.Display;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.baseweather.MainActivity;
import com.example.baseweather.R;
import com.example.baseweather.databinding.ActivitySecondBinding;
import com.example.baseweather.ui.adapter.MyAdapter;

import java.util.Arrays;
import java.util.List;

public class SecondActivity extends AppCompatActivity {

    private ActivitySecondBinding binding;
    private BroadcastReceiver br;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySecondBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initView();
        br = new br();
        registerReceiver(br, new IntentFilter(getPackageName() + "1"));
    }

    private void initView() {

        Integer[] iconId1s = {
                R.drawable.close1, R.drawable.up1, R.drawable.in1, R.drawable.auto1
        };

        Integer[] iconId2s = {
                R.drawable.close2, R.drawable.up2, R.drawable.in2, R.drawable.auto2
        };
        List<Integer> iconList1 = Arrays.asList(iconId1s);
        List<Integer> iconList2 = Arrays.asList(iconId2s);
        binding.rvIcon.setLayoutManager(new GridLayoutManager(this, 4));
        MyAdapter myAdapter = new MyAdapter(iconList1, iconList2);
        binding.rvIcon.setAdapter(myAdapter);

    }


    class br extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            switch (message) {
                case "ON":
                    Intent intent1 = new Intent(SecondActivity.this, MainActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ActivityOptions options =  ActivityOptions.makeBasic();
                    DisplayManager displayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
                    Display display = displayManager.getDisplays()[0];
                    options.setLaunchDisplayId(display.getDisplayId());
                    startActivity(intent1,options.toBundle());
                    break;
            }

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
    }
}
