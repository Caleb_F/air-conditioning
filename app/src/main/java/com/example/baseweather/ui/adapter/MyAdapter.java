package com.example.baseweather.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baseweather.databinding.RvIconItemBinding;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<Integer> preIconList;
    private List<Integer> nextIconList;

    public MyAdapter(List<Integer> preIconList, List<Integer> nextIconList) {
        this.preIconList = preIconList;
        this.nextIconList = nextIconList;
        if (preIconList.size() != nextIconList.size()) {
            throw new RuntimeException("请传入正确长度的icon");
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvIconItemBinding binding = RvIconItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Integer preIcon = preIconList.get(position);
        Integer nextIcon = nextIconList.get(position);
        String preIconStr = preIcon + "";
        String nextIconStr = nextIcon + "";
        holder.binding.iconService.setImageResource(preIcon);
        holder.binding.iconService.setTag(preIconStr);
        holder.binding.iconService.setOnClickListener(v -> {
            ImageView v1 = (ImageView) v;
            String tag = (String) v1.getTag();
            if ((tag.equals(preIconStr))) {
                v1.setImageResource(nextIcon);
                v1.setTag(nextIconStr);
            } else {
                v1.setImageResource(preIcon);
                v1.setTag(preIconStr);
            }
            switch (position) {
                case 0:
                    //        。点击 OFF 按钮空调系统关闭，温控设备停止运行， 页面中央位置提示空调系统已关闭 2 秒后返回中控首页。
                    String message1 = tag.equals(preIconStr) ? "OFF" : "ON";
                    sendBroadCast(v1.getContext(), message1);
                    break;
                case 2:
                    String message = tag.equals(preIconStr) ? "OUT" : "IN";
                    sendBroadCast(v1.getContext(), message);
                    break;
            }

        });
    }

    /**
     * 发送广播
     *
     * @param context
     * @param message
     */
    private void sendBroadCast(Context context, String message) {
        Intent intent = new Intent(context.getPackageName() + "1");
        intent.putExtra("message", message);
        context.sendBroadcast(intent);
    }


    @Override
    public int getItemCount() {
        return preIconList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private RvIconItemBinding binding;

        public MyViewHolder(@NonNull RvIconItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
