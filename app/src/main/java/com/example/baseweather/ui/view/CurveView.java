package com.example.baseweather.ui.view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.baseweather.utils.CommonUtils;

public class CurveView extends View {
    private Paint paint;
    private Path path;


    public CurveView(Context context) {
        super(context);
        init();
    }

    public CurveView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.parseColor("#66CFA8"));
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setAntiAlias(true);
        path = new Path();

        ValueAnimator colorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), Color.parseColor("#66CFA8"), Color.GREEN);
        colorAnimator.setDuration(1000);
        colorAnimator.setRepeatCount(ValueAnimator.INFINITE);
        colorAnimator.addUpdateListener(animation -> {
            int currentColor = (int) animation.getAnimatedValue();
            paint.setColor(currentColor);
            invalidate();
        });
        colorAnimator.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 绘制路径
        canvas.drawPath(path, paint);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width, height;
        int desiredWidth = CommonUtils.dpToPx(this.getContext(), 300);
        int desiredHeight = CommonUtils.dpToPx(this.getContext(), 200);

        //宽度和宽的模式
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        //高度和高的模式
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        //使用具体的值
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        // 高度的测量逻辑
        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        // 开始路径
        setMeasuredDimension(width, height);
    }

    /**
     * 用于编写和长宽有关
     *
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        path.moveTo(0, (float) (h / 8));
        path.quadTo(((float) (w / 4)), 0, ((float) (w / 2)), (float) (h / 8));
        path.quadTo(((float) (w * 3 / 4)), (float) (h / 4), w, (float) (h / 8));


        path.moveTo(0, (float) (h * 3 / 8));
        path.quadTo(((float) (w / 4)), (float) (h / 4), ((float) (w / 2)), (float) (h * 3 / 8));
        path.quadTo(((float) (w * 3 / 4)), (float) (h / 2), w, (float) (h * 3 / 8));

        path.moveTo(0, (float) (h * 5 / 8));
        path.quadTo(((float) (w / 4)), (float) (h / 2), ((float) (w / 2)), (float) (h * 5 / 8));
        path.quadTo(((float) (w * 3 / 4)), (float) (h * 3 / 4), w, (float) (h * 5 / 8));


        path.moveTo(0, (float) (h * 7 / 8));
        path.quadTo(((float) (w / 4)), (float) (h * 3 / 4), ((float) (w / 2)), (float) (h * 7 / 8));
        path.quadTo(((float) (w * 3 / 4)), (float) h, w, (float) (h * 7 / 8));


    }
}