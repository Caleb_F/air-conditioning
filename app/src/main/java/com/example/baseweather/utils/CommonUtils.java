package com.example.baseweather.utils;

import android.content.Context;
import android.widget.Toast;


import com.example.baseweather.MyApplication;
import com.google.gson.Gson;

public class CommonUtils {
    private static final Gson gson = new Gson();

    public static void showToast(String message) {
        MyApplication.getHandler().post(() -> {
            Toast.makeText(MyApplication.getContext(), message, Toast.LENGTH_SHORT).show();
        });
    }

    public static Gson getGson() {
        return gson;
    }

    public static int dpToPx(Context context, int dp) {
        return ((int) (dp * context.getResources().getDisplayMetrics().density));
    }


}

